import { Component, OnInit } from '@angular/core';
import { OFXTransaction } from 'src/app/models/ofx-transaction.model';
import { OFXGridService } from './ofx-grid.service';
import { OFXTransactionTypeEnum } from 'src/app/models/ofx-transaction-type.enum';
import { OFXTransactionCurrencyEnum } from 'src/app/models/ofx-transaction-currency.enum';

@Component({
  selector: 'ofx-grid',
  templateUrl: './ofx-grid.component.html',
  styleUrls: ['./ofx-grid.component.scss']
})
export class OfxGridComponent implements OnInit {


  transactionList: Array<OFXTransaction> = [];

  constructor(private ofxGridService: OFXGridService) { }

  ngOnInit() {
    this.getAllTransactions();
  }

  getAllTransactions(){
    this.ofxGridService.getAllTransactions()
      .subscribe(response => {
        this.transactionList = response;
      });
  }

  getTransactionType(ofxTransactionTypeEnum: OFXTransactionTypeEnum): string{         
    return this.ofxGridService.getTransactionType(ofxTransactionTypeEnum);
  }

  getCurrencyType(ofxTransactionCurrencyEnum: OFXTransactionCurrencyEnum){
    return this.ofxGridService.getCurrencyType(ofxTransactionCurrencyEnum);
  }
}

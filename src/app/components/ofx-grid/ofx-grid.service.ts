import { Injectable } from '@angular/core';
import { HTTPBaseService } from 'src/core/services/http-base.service';
import { OFXTransaction } from 'src/app/models/ofx-transaction.model';
import { Observable } from 'rxjs';
import { OFXTransactionTypeEnum } from 'src/app/models/ofx-transaction-type.enum';
import { OFXTransactionCurrencyEnum } from 'src/app/models/ofx-transaction-currency.enum';

@Injectable({providedIn:'root'})
export class OFXGridService{    

    private readonly GET_URL = "/OFXFile/";
    
    constructor(private httpBaseService: HTTPBaseService) { }

    getAllTransactions(): Observable<Array<OFXTransaction>>{
        return this.httpBaseService.get(this.GET_URL)
    }

    getTransactionType(ofxTransactionTypeEnum: OFXTransactionTypeEnum): string{

        switch (ofxTransactionTypeEnum) {
            case OFXTransactionTypeEnum.CREDIT:
                return "Credit";
            case OFXTransactionTypeEnum.UNDEFINED:
                return "Undefined";
            case OFXTransactionTypeEnum.DEBIT:
                return "Debit";                 
            default:
                return "Undefined"
        }        
    }

    getCurrencyType(ofxTransactionCurrencyEnum: OFXTransactionCurrencyEnum){

        switch (ofxTransactionCurrencyEnum) {
            case OFXTransactionCurrencyEnum.UNDEFINED:
                return "";
            case OFXTransactionCurrencyEnum.BRL:
                return "BRL";
            case OFXTransactionCurrencyEnum.EUR:
                return "EUR";
            case OFXTransactionCurrencyEnum.USD:
                return "USD";
            case OFXTransactionCurrencyEnum.CAD:
                return "CAD";
            case OFXTransactionCurrencyEnum.CNY:
                return "CNY";
            case OFXTransactionCurrencyEnum.ARS:
                return "ARS";
            case OFXTransactionCurrencyEnum.RUB:
                return "RUB";
            case OFXTransactionCurrencyEnum.GBP:
                return "GBP";                       
            default:
                return "";
        }

    }

    

}